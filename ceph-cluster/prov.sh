#!/bin/bash

pushd ..
./install.sh -n ceph1 -m 8192 -d 50G -e 100,100,100,100
./install.sh -n ceph2 -m 8192 -d 50G -e 100,100,100,100
./install.sh -n ceph3 -m 8192 -d 50G -e 100,100,100,100

#./install.sh -n mon1 -m 4096 -d 50G -e 100,100,100
#./install.sh -n mon2 -m 4096 -d 50G -e 100,100,100
#./install.sh -n mon3 -m 4096 -d 50G -e 100,100,100
